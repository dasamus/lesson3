package samus;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle("Circle", 3);
        Square square = new Square("Square", 2);
        Parallelepiped parallelepiped = new Parallelepiped("Parallelepiped", 2, 3, 4);

        Figure[] figures = new Figure[] {circle, square, parallelepiped};

        for (Figure figure: figures) {
            System.out.println(figure.getName());
            System.out.println(figure.getArea());
            if (figure instanceof Parallelepiped) {
                System.out.println(((Parallelepiped) figure).getVolume());
            }
        }

    }
}

//Базовый класс фигура
class Figure {
    private String name;

    public Figure(String name) {
        this.name = name;
    }

    public double getArea(){
        return 0;
    };

    public String getName() {
        return name;
    }
}

//Круг, который наследует базовый класс Figure
class Circle extends Figure {
    private double radius;

    public Circle(String name, double radius) {
        super(name);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea () {
        return Math.PI*radius*radius;
    }
}

//Квадрат, который наследует базовый класс Figure
class Square extends Figure {
    public double side;

    public Square(String name, double side) {
        super(name);
        this.side = side;
    }

    @Override
    public double getArea () {
        return side*side;
    }

    public double getSide() {
        return side;
    }
}

class Parallelepiped extends Square {

    private double height;
    private double length;

    public Parallelepiped(String name, double side, double height, double length) {
        super(name, side);
        this.height = height;
        this.length = length;
    }

    @Override
    public double getArea () {
        return 2*(side*height + side * length + height * length);
    }

    public double getVolume () {
        return side*length*height;
    }
}

