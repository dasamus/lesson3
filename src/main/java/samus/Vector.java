package samus;

public class Vector {
    private double x;
    private double y;
    private double z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double length () {
        return Math.sqrt(scalar(this));
    }

    public double scalar (Vector vector) {
        return Math.sqrt(x * vector.getX() + y * vector.getY() + z * vector.getZ());
    }

    public Vector cross (Vector vector) {
        return new Vector(y*vector.getZ() - z*vector.getY(), z*vector.getX() - x*vector.getZ(), x*vector.getY() - y* vector.getX());
    }

}
